# -*- coding: utf-8 -*-
import os
import subprocess
import urllib

PSCP = 'c:\Program Files (x86)\PuTTY\pscp.exe'
PREFIX = "udp://@"
TEMP = 'temp'


def do(playlist):
    filename = '{0}_udpxy.m3u'.format(playlist)
    with open(filename, 'wb') as fd:
        url = 'http://help.telecom.by/_files/TelecomTV/TelecomTVpacket/{0}.m3u'.format(playlist)
        for line in urllib.urlopen(url):
            if line.startswith(PREFIX):
                line = "http://192.168.1.1:4022/udp/" + line[len(PREFIX):]
            fd.write(line)

    print subprocess.check_output([PSCP, '-scp', filename, 'root@192.168.1.1:/www/iptv'])


def main():
    if not os.path.exists(TEMP):
        os.makedirs(TEMP)
    os.chdir(TEMP)

    for playlist_id in (1, 2, 3):
        do('TVPACKET{0}'.format(playlist_id))

if __name__ == '__main__':
    main()
