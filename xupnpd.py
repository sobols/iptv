# -*- coding: utf-8 -*-
import json
import os
import re
import StringIO
import urllib
from PIL import Image


FILENAME = "TVPACKET3.m3u"
TEMP = 'temp'
KEY_VALUE_STR = r'([a-z\-]+)=("[^"]*"|[^ ]*)'
KEY_VALUE = re.compile(KEY_VALUE_STR)
DESCR = re.compile(r'^#EXTINF:-1(( +{0})+), (.*)$'.format(KEY_VALUE_STR))
THUMBNAIL_SIZE = (100, 100)


class NotGoodChannel(Exception):
    pass


class Channel(object):
    def __init__(self, descr):
        m = DESCR.match(descr)
        if m is None:
            raise NotGoodChannel()

        props = m.group(1)

        self.name = m.groups()[-1].decode('utf-8')
        self.props = {}
        self.url = None
        self.ss_id = None
        self.has_logo = False

        for m in KEY_VALUE.findall(props):
            k, v = m[0].decode('utf-8'), m[1].decode('utf-8')
            if v.startswith(u'"') and v.endswith(u'"'):
                v = v[1:-1]
            self.props[k] = v

    def get_names(self):
        names = [self.name]
        #if 'tvg-name' in self.props:
        #    names.append(self.props['tvg-name'])

        ret = set()
        for name in names:
            ret.add(normalize(name))
            ret.add(normalize(name.replace(u'_', u' ')))
            ret.add(normalize(name.replace(u'-', u' ')))
            ret.add(normalize(name.replace(u'TV 1000', u'TV1000')))
            ret.add(normalize(name.replace(u'(тест)', u'')))
        return ret


def read_channels(stream):
    result = []
    last_channel = None
    for lineno, line in enumerate(stream):
        line = line.strip()
        if line and not line.endswith('#EXTM3U'):
            if line.startswith('udp:'):
                last_channel.url = line
            else:
                try:
                    ch = Channel(line)
                    last_channel = ch
                    result.append(ch)
                except NotGoodChannel:
                    print 'Not parsed:'
                    print line
                    raise
    return result


def spread_groups(channels):
    TAG = 'group-title'
    last_group = None
    for ch in channels:
        cur_group = ch.props.get(TAG)
        if cur_group is not None:
            last_group = cur_group
        elif last_group is not None:
            ch.props[TAG] = last_group


def write_channels(channels, stream):
    print >>stream, '#EXTM3U'
    print >>stream

    for ch in channels:
        props_line = u''.join([u' {0}="{1}"'.format(k, v) for k, v in ch.props.items()])
        print >>stream, u'#EXTINF:-1{0}, {1}'.format(props_line, ch.name).encode('utf-8')
        print >>stream, ch.url
        print >>stream


def normalize(s):
    return s.strip()


def read_ss_iptv_list():
    response = urllib.urlopen('http://api.ss-iptv.com/?action=getChannels').read()
    data = json.loads(response)
    name_to_id = {}
    for x in data:
        title = x['title']
        ss_id = x['id']
        n_title = normalize(title)
        #if n_title in name_to_id:
        #    print 'Warning: duplicate channel', n_title
        name_to_id[n_title] = ss_id
    return name_to_id


def put_ss_ids(channels, name_to_id):
    with open(os.path.join('..', 'mapping.json')) as fd:
        manual_map = json.load(fd)

    for ch in channels:
        mined_ids = []

        for x in ch.get_names():
            if x in name_to_id:
                mined_ids.append(name_to_id[x])
            if x in manual_map:
                trg = manual_map[x]
                if trg in name_to_id:
                    mined_ids.append(name_to_id[trg])

        mined_ids = list(set(mined_ids))
        if len(mined_ids) == 1:
            ch.ss_id = mined_ids[0]
        elif len(mined_ids) == 0:
            print ch.name, ': no logo found'
        else:
            print ch.name, ': multiple logos found'


def make_thumbnail(im, path_to):
    im.thumbnail(THUMBNAIL_SIZE)
    BR = 210
    bg = Image.new('RGB', THUMBNAIL_SIZE, (BR, BR, BR))
    pos = tuple(map(lambda x, y: (x - y) // 2, THUMBNAIL_SIZE, im.size))
    bg.paste(im, pos, im)
    bg.save(path_to, 'JPEG', quality=99, optimize=True, progressive=True, subsampling=0)


def download_logos(channels):
    if not os.path.exists('logos'):
        os.makedirs('logos')
    for ch in channels:
        if ch.ss_id is not None:
            x = ch.ss_id
            url = 'http://api.ss-iptv.com/?action=getChannelImage&channelId={0}'.format(x)
            try:
                print 'Downloading logo for channel', ch.name
                fd = StringIO.StringIO(urllib.urlopen(url).read())
                im = Image.open(fd)
                make_thumbnail(im, os.path.join('logos', '{0}.jpg'.format(x)))
                ch.has_logo = True
            except (KeyboardInterrupt, SystemExit):
                raise
            except:
                print 'FAILED to download logo for', ch.name
                print 'URL', url


def make_logo_links(channels):
    for ch in channels:
        if ch.ss_id is not None and ch.has_logo:
            ch.props['logo'] = u'http://192.168.1.1:4044/logos/{0}.jpg'.format(ch.ss_id)


def dump_data(channels, name_to_id):
    with open('telecom_channels.txt', 'w') as fd:
        for ch in channels:
            print >>fd, u', '.join(ch.get_names()).encode('utf-8')
    with open('ss_iptv_channels.txt', 'w') as fd:
        for k in sorted(name_to_id.keys()):
            print >>fd, k.encode('utf-8')


def main():
    if not os.path.exists(TEMP):
        os.makedirs(TEMP)
    os.chdir(TEMP)

    name_to_id = read_ss_iptv_list()
    urllib.urlretrieve('http://help.telecom.by/_files/TelecomTV/TelecomTVpacket/{0}'.format(FILENAME), 'orig_' + FILENAME)

    with open('orig_' + FILENAME) as f:
        channels = read_channels(f)

    #dump_data(channels, name_to_id)

    spread_groups(channels)
    put_ss_ids(channels, name_to_id)

    download_logos(channels)
    make_logo_links(channels)

    with open('{0}'.format(FILENAME), 'w') as f:
        write_channels(channels, f)

if __name__ == '__main__':
    main()
